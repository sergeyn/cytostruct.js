define(function(require) {
  return function(cytostruct, sali, Logger, _dataobj, utils, fields) {
    var logger = Logger.get("blogic");

    var default_layout = { name: 'preset' };
    var basecgi = "get.php?action=fetch";
    // var struct_type = "cif";

    // element fields mapping
    var _d_Res1 = "Res1";
    var _d_Res2 = "Res2";
    var _d_Ali1 = "Ali1";
    var _d_Ali2 = "Ali2";

    if(window.cytostructjs.showing) {
       //RESET
       //   $(cytostruct.tooltip).hide();
    } else {
      cytostruct.sali_win = utils.make_alignment_window();
      //cytostruct.tooltip = utils.make_tooltip();
    }

    //-----------------------------------------------------------------------
    // when hovering inside mol. viewer
    // var default_hover = function(picking){
    //   if (picking && (picking.atom || picking.bond)) {
    //     var atom = picking.atom || picking.closestBondAtom
    //
    //     cytostruct.alignment.select(atom.structure.id, atom.resno);
    //     cytostruct.color_residue(atom.resno, atom.structure.id);
    //
    //     let mp = picking.mouse.position;
    //     let offset = cytostruct.molv_coordinates();
    //     cytostruct.tooltip.innerText =
    //       "Residue: " + atom.resname + "/" +
    //       dataobj.astrals[atom.resname] +
    //       "(" + atom.resno + "~" + atom.residueIndex + ")";
    //     cytostruct.tooltip.style.bottom = window.innerHeight - offset.top - mp.y + 3 + "px"
    //     cytostruct.tooltip.style.left = offset.left + mp.x + 3 + "px"
    //     cytostruct.tooltip.style.display = "block"
    //   } else {
    //     cytostruct.tooltip.style.display = "none"
    //   }
    // };
    //-----------------------------------------------------------------------
    // clicking node or edge in cytoscape
    var default_handler = function(evt){
      // cytostruct.remove_all_comps();
      var cyelement = evt.target;
      // cytostruct.change_molviewer_title(cyelement.data().name);

      if(cyelement.group() === "edges") {
        var srcid = cyelement.source().data().name.split("_")[0]; //.toLowerCase();
        var trgtid = cyelement.target().data().name.split("_")[0]; //.toLowerCase();
        var cyeldata = cyelement.data();
        cytostruct.alignment = new sali(
          $(cytostruct.sali_win),
          srcid,
          trgtid,
          cyeldata[_d_Ali1],
          cyeldata[_d_Ali2],
          parseInt(cyeldata[_d_Res1].split("-")[0]),
          parseInt(cyeldata[_d_Res2].split("-")[0]));
        cytostruct.alignment.render();
        //cytostruct.alignment.set_on_hover(cytostruct.color_residue);
        //cytostruct.mv_set_on_hover(default_hover);
      }

      // cytostruct.load_files(cytostruct.form_url(cyelement), struct_type)
      //  .then(function(structs) {
      //     return cytostruct.load_structures(structs)})
      //       .then(function(structs){
      //         let resList = [
      //           String(cyelement.data()[_d_Res1]).replace(/\:/g,"-").replace(/,/g," or "),
      //           String(cyelement.data()[_d_Res2]).replace(/\:/g,"-").replace(/,/g," or ")];
      //         cytostruct.represent_components(structs, resList);
      //       });
      //
      //   if(cyelement.group() === "edges") {
      //     var srcid = cyelement.source().data().name.split("_")[0]; //.toLowerCase();
      //     var trgtid = cyelement.target().data().name.split("_")[0]; //.toLowerCase();
      //     var cyeldata = cyelement.data();
      //     cytostruct.alignment = new sali(
      //       $(cytostruct.sali_win),
      //       srcid,
      //       trgtid,
      //       cyeldata[_d_Ali1],
      //       cyeldata[_d_Ali2],
      //       parseInt(cyeldata[_d_Res1].split("-")[0]),
      //       parseInt(cyeldata[_d_Res2].split("-")[0]));
      //     cytostruct.alignment.render();
      //     cytostruct.alignment.set_on_hover(cytostruct.color_residue);
      //     //cytostruct.mv_set_on_hover(default_hover);
      //   }
      //   else {
      //     cytostruct.mv_set_on_hover(function(_p){});
      //   }
      };
      //-----------------------------------------------------------------------
      // mixin mol.viewer and cytoscape methods to cytostruct
      //Object.assign(cytostruct, cytostruct.molviewer);

      Object.assign(cytostruct, cytostruct.cyviewer);

      load_from_json = function(cytohandle, net) {
        layout = default_layout;
        cytohandle.cs_embed_in_div("cytostruct", net.elements, net.style, layout, {lens: true});
        cytohandle.set_tap_node(default_handler);
        cytohandle.set_tap_edge(default_handler);
        window.cytostructjs.showing = true;
      }

      construct_static_name_and_url = function(fields, cgibase) {
        name = fields["db"] + "-" + fields["net"];
        eval = ("eval" in fields) ? fields["eval"] : 0.1;
        len = ("len" in fields) ? fields["len"] : 20;
        static_name = name + "__" + eval + "__" + len;
        uri = cgibase
              + "&target=" + fields["net"]
              + "&db=" + fields["db"]
              + "&eval=" + eval
              + "&len=" + len;
        logger.debug("Candidate: " + static_name + " aka " + uri);
        return [static_name, uri];
      }

      get_with_ajax = function(path, callmeback, failcall) {
        $.getJSON(path)
        .done(function(data) {
          logger.debug("$.getJSON success");
          callmeback(data);
        })
        .fail(failcall);
      };

      fetch_json = function(fields, cgibase, callmeback, failcall) {
        // 1. try static resource (with require)
        // 2. if not avaiable, try to call a cgi to fetch it with $.getJSON
        //    the cgi may be able to create a static copy in seconds
        //    3.1 if $.getJSON was a success, use the data
        //    3.2 if $.getJSON fails, .fail(function( jqxhr, textStatus, error )
        path_arr = construct_static_name_and_url(fields, cgibase);
        if ("force" in fields) {
          get_with_ajax(path_arr[1], callmeback, failcall);
        }
        else {
          require([path_arr[0]],
            function(data) { callmeback(data); },
            function(err) {
              logger.info("Require failed with: " + err);
              get_with_ajax(path_arr[1], callmeback, failcall);
          });
        }
      }

      if(window.cytostructjs.showing) {
        alert("reset");
      }
      else {
        fetch_json(fields, basecgi,
                   function(net) { load_from_json(cytostruct, net); },
                   function(jqxhr, _textStatus, _error) {
                     console.log(jqxhr.status);
                     alert( "Could not get the network. Try again later." );
                   });
      }
  };
})
