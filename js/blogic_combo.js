define(["toolbar"], function(toolbar) {
  return function(cytostruct, _sali, Logger, _dataobj, utils, fields) {
    var logger = Logger.get("blogic");

    var default_layout = { name: 'preset' };
    var basecgi = "get.php?action=fetch";

    //-----------------------------------------------------------------------
    // clicking node or edge in cytoscape
    var default_handler = function(evt){
      var cyelement = evt.target;
      if(cyelement.group() === "nodes") {
        populate_lens(cyelement);
      }
    };

     //-----------------------------------------------------------------------
    var populate_lens = function(node) {
      let arr_neigh = node.connectedEdges().map(function(edge) {
        let src = edge.source();
        let tgt = edge.target();
        let other = (src === node) ? tgt : src;
        return {name: other.data().name, value: edge.data().value };
      }).sort(function(a,b) { return b.value - a.value;}).map(function(obj){
        return "<li><a href='http://scop.berkeley.edu/sccs='" + obj.name + ">" + obj.name + "</a>&nbsp;" + obj.value + "</li>";
      });

      // if shift
      let [lens_win, lens_header, lens_body] = utils.get_from_registry("lens");
      let name = node.data().name;
      lens_header.html(name);
      lens_body.html(
        "<p>Node value: "
        + node.data().count
        + "<br/><a href='http://scop.berkeley.edu/sccs="
        + name + "'>SCOP</a></p><div><ul style='list-style-type: none'"
        + arr_neigh.join("\n")
        + "</ul></div>");
      lens_win.show();
    };

    //-----------------------------------------------------------------------
    var build_search_bar = function(toggler) {
      let search =  $('<input type="button" value="&larr; Zoom to fold">');
      let input = $('<input type="text">');
      let search_handler = function() {
        cytostruct.toggle_neighbors({checked: false});
        let node = cytostruct.cyviewer.lookup_selector('node[name = "' + input.val() + '"]');
        if(node) {
          cytostruct.cyviewer.center(node);
          cytostruct.cyviewer.zoom({ level: 2, position: node.position()});
          node.select();
          toggler.prop("checked", true);
          cytostruct.cyviewer.show_only_neighbors();
          populate_lens(node);
        }
      };
      input.on('keyup', function(e) { if(e.keyCode == 13) { search_handler(); }});
      search.click(search_handler);

      let spn = $('<span></span>');
      spn.append(input);
      spn.append(search);
      return spn;
    };

    //-----------------------------------------------------------------------
    var build_class_toggler = function() {
      let tb = new toolbar(cytostruct);
      let classes1 = {"a" : "red", "b" : "blue", "c" : "green", "d" : "orange"};
      let classes2 = {"e" : "magenta", "f" : "cyan", "g" : "silver"};
      let spn = $('<span></span>')
      let prepare = function(classes, key, isChecked) {
        tb.appendToggler(spn, key, classes[key], isChecked, function(isC, val){
          if(isC) {
            cytostruct.cyviewer.hide_for_selector('node[color = "' + val + '"]');
          }
          else {
            cytostruct.cyviewer.show_for_selector('node[color = "' + val + '"]');
          }
        })
      };
      Object.keys(classes1).forEach(function(key,_index) { prepare(classes1, key, true) });
      Object.keys(classes2).forEach(function(key,_index) { prepare(classes2, key, false) });
      return spn;
    };

    //-----------------------------------------------------------------------
    var add_node_togglers = function(cytostruct, toolbar_div) {
      let tb = new toolbar(cytostruct);
      return [ tb.appendToggler(
                   toolbar_div,
                   "Hide orphans", 0, false,
                   function(isC, _){
                     cytostruct.toggle_orphans({checked: !isC}); }),
              tb.appendToggler(
                  toolbar_div,
                  "Hide non-neighbors", 1, false,
                  function(isC, _){
                    cytostruct.toggle_neighbors({checked: !isC}); })
              ];
    };

    //-----------------------------------------------------------------------
    var populate_toolbar = function(cytostruct) {
      let toolbar_div = utils.get_from_registry("toolbar");
      let node_togglers = add_node_togglers(cytostruct, toolbar_div);

      toolbar_div.append(build_search_bar(node_togglers[1]));
      // cytostruct.set_on("unselect", "node", function() {
      //   //node_togglers[1].prop("checked", false);
      //   //cytostruct.cyviewer.show_non_neighbors();
      // });
      toolbar_div.append($("<label>Toggle classes:</label>"));
      toolbar_div.append(build_class_toggler());
      utils.add_to_registry("lens", utils.make_lens_window());

      cytostruct.cyviewer.hide_for_selector('node[color = "magenta"]');
      cytostruct.cyviewer.hide_for_selector('node[color = "cyan"]');
      cytostruct.cyviewer.hide_for_selector('node[color = "silver"]');
    };

    //-----------------------------------------------------------------------
    var load_from_json = function(cytohandle, net) {
      layout = default_layout;
      cytohandle.cs_embed_in_div("cytostruct", net.elements, net.style, layout, {lens: true});
      populate_toolbar(cytostruct);
      cytohandle.set_tap_node(default_handler);
      cytohandle.set_tap_edge(default_handler);
      window.cytostructjs.showing = true;

    };

    //-----------------------------------------------------------------------
    var construct_static_name_and_url = function(fields, cgibase) {
      name = fields["db"] + "-" + fields["net"];
      eval = ("eval" in fields) ? fields["eval"] : 0.1;
      len = ("len" in fields) ? fields["len"] : 20;
      static_name = name + "__" + eval + "__" + len;
      uri = cgibase
            + "&target=" + fields["net"]
            + "&db=" + fields["db"]
            + "&eval=" + eval
            + "&len=" + len;
      logger.debug("Candidate: " + static_name + " aka " + uri);
      return [static_name, uri];
    };

    //-----------------------------------------------------------------------
    var get_with_ajax = function(path, callmeback, failcall) {
      $.getJSON(path)
          .done(function(data) {
            logger.debug("$.getJSON success");
            callmeback(data);
          })
          .fail(failcall);
    };

    //-----------------------------------------------------------------------
    var fetch_json = function(fields, cgibase, callmeback, failcall) {
      // 1. try static resource (with require)
      // 2. if not avaiable, try to call a cgi to fetch it with $.getJSON
      //    the cgi may be able to create a static copy in seconds
      //    3.1 if $.getJSON was a success, use the data
      //    3.2 if $.getJSON fails, .fail(function( jqxhr, textStatus, error )
      path_arr = construct_static_name_and_url(fields, cgibase);
      if ("force" in fields) {
        get_with_ajax(path_arr[1], callmeback, failcall);
      }
      else {
        require(["net/" + path_arr[0]],
          function(data) { callmeback(data); },
          function(err) {
            logger.info("Require failed with: " + err);
            get_with_ajax(path_arr[1], callmeback, failcall);
        });
      }
    };

    // <<<<< Main <<<<
    // mixin cytoscape viewer methods to cytostruct
    Object.assign(cytostruct, cytostruct.cyviewer);
    fetch_json(fields, basecgi,
               function(net) { load_from_json(cytostruct, net); },
               function(jqxhr, _textStatus, _error) {
                 logger.info(jqxhr.status);
                 alert( "Could not get the network. Try again later." );
               });
  };
})
