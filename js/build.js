({
    baseUrl: "lib",
    paths: {
	    jquery: "empty:",
	    cytoscape: "empty:",
	    logger: "empty:",
	    interact: "empty:",
	    NGL: "empty:",
	    nouislider: "empty:",
    },
    name: "config",
    out: "cytostructb.js"
})
