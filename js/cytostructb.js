
define('sali',[],function() {
  const GAP = "-";

  // produce a span html string from 2 opposing residues <residue, other>
  let residue = function(index, res, other) {
    let cls = "residue";
    if(res === other) {
      cls += " residue_identical";
    }
    return `<span id="${index}" class="${cls}">${res}</span>`;
  };

  // calculate "real" offset, ignoring gaps
  // NB: could be done ahead of time and cached
  let correct_offset = function(index, start, residues_arr) {
    // residues are 1-based, but so are the offsets
    for(let i in residues_arr) {
      if(start === index){
        return i;
      }
      if(residues_arr[i] !== GAP) {
        ++start;
      }
    }
  };

  return function(element, lname, rname, lresi, rresi, loffset, roffset) {
    var _model = {
      l_residues: lresi.split(""),
      r_residues: rresi.split(""),
      l_offset: loffset,
      r_offset: roffset,
      l_name: lname,
      r_name: rname,
      domelement: element,
      onhover: function(){},
      onclick: function(){}
    };

    return {
      // remove existing alignement from the div
      clear: function() {
        let element = _model.domelement;
        element.html("<div id=\"lresidues\"></div><div id=\"rresidues\"></div>");
      },

      // Draw the alignment in the given element
      render: function() {
        const lresidues = _model.l_residues.map(function(res, index){
          return residue("lrsd_" + index, res, _model.r_residues[index]);});
        const rresidues = _model.r_residues.map(function(res, index){
          return residue("rrsd_" + index, res, _model.l_residues[index]);});

        let element = _model.domelement;
        element.html(
          `<div id="lresidues">${lresidues.join("")}</div>
           <div id="rresidues">${rresidues.join("")}</div>`);
        element.off();
        element.on("mouseenter", ".residue", {obj: this},
          function(event) {
            let idarr = event.target.id.split("_");
            let l_or_r = idarr[0][0];
            let saliself = event.data.obj;
            let offset = saliself.offset(l_or_r);
            saliself.call_onhover(offset + parseInt(idarr[1]), saliself.name(l_or_r));
          });
      },

      // if resnum<0 just clear
      select: function(id, resnum) {
        let res_data =
          id === _model.l_name ?
            [ _model.l_offset, _model.l_residues] :
            [ _model.r_offset, _model.r_residues];

        $(".residue").removeClass("residue_hover");
        if(resnum >= 0) {
          let offset = correct_offset(resnum, res_data[0], res_data[1]);
          $("#lresidues :eq(" + offset + ")").addClass("residue_hover");
          $("#rresidues :eq(" + offset + ")").addClass("residue_hover");
        }
      },

      name: function(left_or_right) {
        return left_or_right === "l" ? _model.l_name : _model.r_name;
      },

      offset: function(left_or_right) {
        return left_or_right === "l" ? _model.l_offset : _model.r_offset;
      },

      // return a pair of residues [l,r] - use "local" 0-based index
      at: function(index) { return _model.l_residues[index],
      _model.r_residues[index]; },
      // return the residue at absolute offset (FASTA index)
      left_at: function(offset) {
        return _model.l_residues[offset - _model.l_offset];},
      right_at: function(offset) {
        return _model.r_residues[offset - _model.r_offset];},
      length: function() { return _model.l_residues.length;},

      call_onhover: function(offset, name) { _model.onhover(offset, name); },
      set_on_hover: function(callback) { _model.onhover = callback; },
      set_on_click: function(callback) { _model.onclick = callback; }
    };
  };
});

define('data',['require'],function(require) {
  const ASTRALS = {
    "ALA" : "A", "VAL" : "V", "PHE" : "F", "PRO" : "P", "MET" : "M", "ILE" : "I", "LEU" : "L", "ASP" : "D", "GLU" : "E", "LYS" : "K",
    "ARG" : "R", "SER" : "S", "THR" : "T", "TYR" : "Y", "HIS" : "H", "CYS" : "C", "ASN" : "N", "GLN" : "Q", "TRP" : "W", "GLY" : "G",
    "UNK" : "X", "2AS" : "D", "3AH" : "H", "5HP" : "E", "ACL" : "R", "AGM" : "R", "AIB" : "A", "ALM" : "A", "ALO" : "T", "ALY" : "K",
    "ARM" : "R", "ASA" : "D", "ASB" : "D", "ASK" : "D", "ASL" : "D", "ASQ" : "D", "ASX" : "B", "AYA" : "A", "BCS" : "C", "BHD" : "D",
    "BMT" : "T", "BNN" : "A", "BUC" : "C", "BUG" : "L", "C5C" : "C", "C6C" : "C", "CCS" : "C", "CEA" : "C", "CGU" : "E", "CHG" : "A",
    "CLE" : "L", "CME" : "C", "CSD" : "A", "CSO" : "C", "CSP" : "C", "CSS" : "C", "CSW" : "C", "CSX" : "C", "CXM" : "M", "CY1" : "C",
    "CY3" : "C", "CYG" : "C", "CYM" : "C", "CYQ" : "C", "DAH" : "F", "DAL" : "A", "DAR" : "R", "DAS" : "D", "DCY" : "C", "DGL" : "E",
    "DGN" : "Q", "DHA" : "A", "DHI" : "H", "DIL" : "I", "DIV" : "V", "DLE" : "L", "DLY" : "K", "DNP" : "A", "DPN" : "F", "DPR" : "P",
    "DSN" : "S", "DSP" : "D", "DTH" : "T", "DTR" : "W", "DTY" : "Y", "DVA" : "V", "EFC" : "C", "FLA" : "A", "FME" : "M", "GGL" : "E",
    "GL3" : "G", "GLX" : "Z", "GLZ" : "G", "GMA" : "E", "GSC" : "G", "HAC" : "A", "HAR" : "R", "HIC" : "H", "HIP" : "H", "HMR" : "R",
    "HPQ" : "F", "HTR" : "W", "HYP" : "P", "IIL" : "I", "IYR" : "Y", "KCX" : "K", "LLP" : "K", "LLY" : "K", "LTR" : "W", "LYM" : "K",
    "LYZ" : "K", "MAA" : "A", "MEN" : "N", "MHS" : "H", "MIS" : "S", "MLE" : "L", "MPQ" : "G", "MSA" : "G", "MSE" : "M", "MVA" : "V",
    "NEM" : "H", "NEP" : "H", "NLE" : "L", "NLN" : "L", "NLP" : "L", "NMC" : "G", "OAS" : "S", "OCS" : "C", "OMT" : "M", "PAQ" : "Y",
    "PCA" : "E", "PEC" : "C", "PHI" : "F", "PHL" : "F", "PR3" : "C", "PRR" : "A", "PTR" : "Y", "SAC" : "S", "SAR" : "G", "SCH" : "C",
    "SCS" : "C", "SCY" : "C", "SEL" : "S", "SEP" : "S", "SET" : "S", "SHC" : "C", "SHR" : "K", "SMC" : "C", "SOC" : "C", "STY" : "Y",
    "SVA" : "S", "TIH" : "A", "TPL" : "W", "TPO" : "T", "TPQ" : "A", "TRG" : "K", "TRO" : "W", "TYB" : "Y", "TYQ" : "Y", "TYS" : "Y",
    "TYY" : "Y"
  };
  return { astrals: ASTRALS };
});


define('utils',["jquery", "interact", "logger"],
  function($, interact, Logger) {
    const logger = Logger.get("cytostruct");
    let win_registry = new Map();

    let _handle = {

      // check if window registry has one with given name
      has_registry: function(name) { return win_registry.hasOwnProperty(name); },
      // fetch a window structure from the registry
      get_from_registry: function(name) { return win_registry[name];},
      // add window structure to registry under given name
      add_to_registry: function(name, element_data) { win_registry[name] = element_data; },
      // remove
      rm_from_registry: function(name) { delete win_registry[name]; },

      //--------------------------------------------------------------------------
      // run nillary/unary jquery method on a window
      // examples:
      // jq_func("hide", "lens")
      // jq_func("append", "toolbar", $("<div>foo</div>"));
      jq_func: function(func, win_name, arg = {}) {
        if(this.has_registry(win_name)) {
          $(this.get_from_registry(win_name))[func](arg);
        }
        else {
          logger.debug("No window '" + win_name + "' registered");
        }
      },
      //--------------------------------------------------------------------------
      // given an $(element), make it draggable, resizable with header
      // stopcb is a callback to run when resizing of the element stops
      // returns the window structure -- [win, header, element, close]
      make_window: function (element, stopcb) {
        logger.debug("Making window from ", element);
        let win = $("<div class=\"draggable viewportwindow\"></div>");
        let header = $("<span></span>");
        let close = $("<u>close</u>");
        close.css({"float": "right", "cursor" : "pointer"});
        close.click(function(){win.hide();}); // default behavior is to hide
        let hwrap = $("<div class=\"panel-heading\"></div>");
        win.css({"z-index": 100 + win_registry.length, "position": "absolute", "top" : "200px"});
        hwrap.append(header);
        hwrap.append(close);
        win.prepend(hwrap);
        element = $(element);
        win.append(element);
        win.height(element.height() + 40);
        win.width(element.width() + 16);
        $("body").append(win);

        let translateWin = function(target, x, y) {
          // translate the element
          target.style.webkitTransform =
          target.style.transform =
            "translate(" + x + "px, " + y + "px)";
        };
        let dragMoveListener =  function(event) {
          var target = event.target,
            // keep the dragged position in the data-x/data-y attributes
            x = (parseFloat(target.getAttribute("data-x")) || 0) + event.dx,
            y = (parseFloat(target.getAttribute("data-y")) || 0) + event.dy;
            // update the posiion attributes
          target.setAttribute("data-x", x);
          target.setAttribute("data-y", y);

          translateWin(target, x, y);
        };

        interact(win.get()[0])
          .draggable({
            allowFrom: ".panel-heading",
            restrict: {
              restriction: "parent",
            },
            onmove: dragMoveListener
          })
          .resizable({
            edges: { right: true, bottom: true }
          })
          .on("resizemove", function (event) {
            var target = event.target,
              x = (parseFloat(target.getAttribute("data-x")) || 0),
              y = (parseFloat(target.getAttribute("data-y")) || 0);

            // update the element's style
            target.style.width  = event.rect.width + "px";
            target.style.height = event.rect.height + "px";

            // translate when resizing from top or left edges
            x += event.deltaRect.left;
            y += event.deltaRect.top;

            target.style.webkitTransform = target.style.transform =
                "translate(" + x + "px," + y + "px)";

            target.setAttribute("data-x", x);
            target.setAttribute("data-y", y);
            stopcb(event);
          });
        translateWin(win.get()[0], 10*(win_registry.length-1)+3, 10*(win_registry.length-1)+3);
        return [win, header, element, close];
      },
      //--------------------------------------------------------------------------
      // specifically create a lens window
      make_lens_window: function() {
        let lendiv = $("<div></div>");
        lendiv.css({display: "block", width: "400px", height: "500px", "z-index": 101, "overflow-y" : "auto"});
        $("body").append(lendiv);
        let [win, lheader, body, close] = this.make_window(lendiv, function(e) {
          lendiv.width(e.rect.width);
          lendiv.height(e.rect.height-10);
        });
        lheader.html("Lens");
        win.hide();
        this.add_to_registry("lens", win);
        this.add_to_registry("lens_header", lheader);
        return [win, lheader, body, close];
      },
      //--------------------------------------------------------------------------
      // create alignment window
      make_alignment_window: function() {
        let saliwin = document.createElement("div");
        Object.assign(saliwin.style, {
          width: "100%",
          height: "60px",
          "overflow-x": "scroll",
          "overflow-y": "hidden",
          "white-space": "nowrap"
        });
        document.body.appendChild(saliwin);
        let [win, lheader, body, close] = this.make_window(saliwin, function() {});
        lheader.html("Alignment view");
        win.width(450); win.height(85);
        return [win, lheader, body, close];
      },
      sali_default_handler: function(cyelement, sali /*, _onhover*/) {
        // element fields mapping
        let _d_Res1 = "Res1";
        let _d_Res2 = "Res2";
        let _d_Ali1 = "Ali1";
        let _d_Ali2 = "Ali2";

        if(!this.has_registry("sali_win")) {
          let elements = this.make_alignment_window();
          this.add_to_registry("sali_win", elements);
          let self = this;
          elements[3].click(function() {self.rm_from_registry("sali_win");});
        }
        let srcid = cyelement.source().data().name.split("_")[0]; //.toLowerCase();
        let trgtid = cyelement.target().data().name.split("_")[0]; //.toLowerCase();
        let cyeldata = cyelement.data();
        let alignment = new sali(
          $(this.get_from_registry("sali_win")[2]),
          srcid,
          trgtid,
          cyeldata[_d_Ali1],
          cyeldata[_d_Ali2],
          parseInt(cyeldata[_d_Res1].split("-")[0]),
          parseInt(cyeldata[_d_Res2].split("-")[0]));
        alignment.render();
        //cytostruct.alignment.set_on_hover(_onhover) <- cytostruct.color_residue)
        //cytostruct.mv_set_on_hover(default_hover);
      },
      //--------------------------------------------------------------------------
      // create tooltip window
      make_tooltip: function() {
        // create tooltip element and add to document body
        var tooltip = document.createElement("div");
        Object.assign(tooltip.style, {
          display: "none",
          position: "fixed",
          zIndex: 101,
          pointerEvents: "none",
          backgroundColor: "rgba( 0, 0, 0, 0.6 )",
          color: "lightgrey",
          padding: "8px",
          fontFamily: "sans-serif"
        });
        document.body.appendChild(tooltip);
        return tooltip;
      },
      //--------------------------------------------------------------------------
      // convert location hash to, well, a hash
      location_fields: function() {
        let fields = {};
        location.hash.replace("#","").split("&")
          .map(x => x.split("=")).forEach(x => fields[x[0]] = x[1]);
        return fields;
      },
      // and back
      fields_to_location: function(fields) {
        location.hash = "#" + Object.entries(fields).map(x => x.join("=")).join("&");
      },
      //--------------------------------------------------------------------------
      // get all evalues of edges as an array
      get_evalues_from_elements: function(e_json, evalue_tag = "evalue") {
        if(e_json.hasOwnProperty("edges")) {
          return e_json["edges"]
            .map(edge => edge["data"][evalue_tag])
            .sort(function(a,b) { return a - b;});
        }
        return [0, 1000];
      }
    };
    return _handle;
  });

/*global requirejs*/
/*global g_blogic*/
requirejs.config({
  baseUrl: "js/lib/",
  paths: {
    jquery:     "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min",
    cytoscape:  "https://cdnjs.cloudflare.com/ajax/libs/cytoscape/3.5.0/cytoscape.min",
    interact:   "https://unpkg.com/interactjs@next/dist/interact.min",
    NGL:        "https://unpkg.com/ngl@0.10.4/dist/ngl",
    logger:     "https://unpkg.com/js-logger/src/logger.min",
    nouislider: "https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/13.1.2/nouislider.min",
    list:       "https://cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min",
    net: "../../networks",
    aux: "../../aux"
  }
});

define(["cytostruct", "sali", "logger", "data", "utils"],
  function (cytostructC, sali, Logger, dataobj, utils) {
    Logger.useDefaults();
    if (document.cookie.indexOf("LOGVERBOSE") != -1) {
      Logger.setLevel(Logger.DEBUG);
    }
    else {
      Logger.setLevel(Logger.INFO);
    }

    require([g_blogic], function(blogic){
      // register cytostruct globally for debugging
      window.cytostructjs = {
        logic: blogic,
        cytostruct: cytostructC,
        seq_ali: sali,
        logger: Logger,
        data: dataobj,
        utilities: utils
      };
      let fields = utils.location_fields();
      blogic(cytostructC, sali, Logger, dataobj, utils, fields, "css");
    });
  });

