/*global requirejs*/
/*global g_blogic*/
requirejs.config({
  baseUrl: "js/",
  paths: {
    net: "../../networks",
    aux: "../../aux"
  },
});

define(["cytostructb"],
  function(md) {
    // register cytostruct globally (for debugging and stuff)
    window.cytostructjs = md;
    const logger = md.logger.get("main");
    const fields = md.utilities.location_fields();
    let empty = function(){};
    // ! convention alert. If a global g_blogic is defined...
    // 1) and it is a string: assume it is a name of a .js file to load
    // 2) and it is a function: execute it, passing all the modules
    // Only if undefined, return an array of all the modules.
    if (typeof g_blogic === "undefined") {
      logger.error("No b.logic");
      return empty;
    }
    else if (typeof g_blogic === "function") {
      logger.info("Invoking the provided b.logic function");
      g_blogic(md.cytostruct, md.seq_ali, md.logger, md.data, md.utilities, fields, "css");
      return empty;
    }
    logger.info(`"Fetching ${g_blogic}.js with require`);
    require([g_blogic], function(blogic){
      window.cytostructjs.logic =  blogic;
      blogic(md.cytostruct, md.seq_ali, md.logger, md.data, md.utilities, fields, "css");
      return empty;
    });
  }
);
