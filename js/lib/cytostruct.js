"use strict";
define(["jquery", "utils", "molviewer", "cyviewer", "nouislider", "logger", "windowing", "file_loading"],
  function($, utils, molviewer_module, cyviewer_module, nouislider, Logger, Windowing, FLoader){
    const logger = Logger.get("cytostruct");
    const _default_cy_id = "cy";
    const _default_mv = "mv";

    // NB: currently, supporting only single mol. viewer panel
    // and cytostruct itself is a "singleton". Will make a proper class one day.
    const _default_viewport_id = "viewport";
    let removed = [];
    let win_owner = new Windowing();

    let handle = {
      version: "0.2.1",
      alignment:  {},
      removed: [],
      floader: new FLoader(molviewer_module.get_ngl()),

      // getter for file loader used by cytostruct
      get_file_loader() {
        return this.floader;
      },
      //------------------------------------------------------------------------
      // getter for the windows manager used by all components
      get_win_manager() {
        return win_owner;
      },
      //------------------------------------------------------------------------
      // change the title of (active) floating mol. viewer
      change_molviewer_title(title_html) {
        win_owner.get_from_registry(_default_mv)[1].html(title_html);
      },
      //------------------------------------------------------------------------
      // get document position of the mol.viewer window header
      molv_coordinates() {
        return win_owner.get_from_registry(_default_mv)[1].offset();
      },
      //------------------------------------------------------------------------
      // override default handler for network node click
      set_tap_node(callback) {
        this.cy_on("tap", "node", callback);
      },
      //------------------------------------------------------------------------
      // override default handler for network edge click
      set_tap_edge(callback) {
        this.cy_on("tap", "edge", callback);
      },
      //------------------------------------------------------------------------
      // expose setting handlers for all events
      set_on(what, where, callback) {
        this.cy_on(what, where, callback);
      },
      //------------------------------------------------------------------------
      // handlers for ui elements
      toggle_neighbors(obj) {
        if(obj.checked) { this.cy_show_only_neighbors(); }
        else { this.cy_show_non_neighbors();}
      },
      toggle_orphans: function(obj) {
        if(obj.checked) { this.cy_hide_orphans(); }
        else { this.cy_show_orphans();}
      },

      _add_slider: function(toolbar, elements_json) {
        toolbar.append($("<div>E-value threshold:<span id=\"evalthr\"></span><div id=\"slider\" style=\"width: 500px; margin: 10px\"></div></div>"));
        let slider = document.getElementById("slider");
        let values = utils.get_evalues_from_elements(elements_json);
        nouislider.create(slider,
          {
            range: {
              "min": [0],
              "max": [values.length - 1]
            },
            start: values.length - 1,
            step: 1
          });
        let cyv = this;
        let slidecb = function() {
          let threshold = values[Math.floor(slider.noUiSlider.get())];
          $("#evalthr").text(threshold);
          removed.restore();
          let elements = cyv.cy_filter(`edge[evalue>${threshold}]`);
          removed = elements.remove();
          logger.debug("Removed " + removed.length + " edges");
        };
        slider.noUiSlider.on("change", slidecb);
      },

      _add_mv: function(root) {
        root.append($("<div id=\"" + _default_viewport_id + "\"></div>"));
        const win_data =
          win_owner.add_to_registry(
            _default_mv,
            win_owner.make_window(
              $("#" + _default_viewport_id),
              e => this.mv_set_size(e.rect.width, e.rect.height - 20 /* compensate for header */)));
        const panel = win_data[0];
        panel.css({display: "block", width: "400px", height: "420px", "z-index": 100});
        win_data[1].html("Molecular viewer");
        this.mv_embed_in_div(_default_viewport_id);

        win_owner.add_to_registry("tooltip", utils.make_tooltip(win_owner));
      },
      //------------------------------------------------------------------------
      // embed the network in given div, maybe prepare floating molviewer, toolbar
      // TODO: proper win config documentation
      // options: slider, _default_mv aka mv,
      cs_embed_in_div(div_id, elements_json, style_json, layout_js, win_config) {
        logger.debug("Embedding cy2struct in ", div_id);
        let height = Math.floor(window.innerHeight * 0.9); // TODO: why not the div?
        let root = $("<div id=\"" + div_id + "_c2s\"  style=\"width: 90%; height: " + height + "px;\"></div>");
        let toolbar = $("<div id=\"tools\" style=\"z-index: 200; height: 100px;\"></div>");
        $(div_id).append(toolbar);
        $(div_id).append(root);
        win_owner.add_to_registry("toolbar", toolbar);

        if (win_config.hasOwnProperty("slider")) {
          // TODO: provide a slider callback to be called with threshold value...
          this._add_slider(toolbar, elements_json);
        }
        if (win_config.hasOwnProperty(_default_mv)) {
          this._add_mv(root);
        }

        let cy_win = $("<div style=\"height: 100%\"><div style=\"height: 100%;\" id=\"" + _default_cy_id + "\"></div></div>");
        root.append(cy_win);

        this.cy_embed_in_div($("#" + _default_cy_id), elements_json, style_json, layout_js);
        let elements = this.cy_filter("edge[evalue<0]");
        removed = elements.remove();
      },
    };

    // mix-in molecular viewer and network viewer functionality
    return Object.assign({}, handle, molviewer_module, cyviewer_module);
  });
