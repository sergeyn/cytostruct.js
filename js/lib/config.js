/*global requirejs*/
requirejs.config({
  baseUrl: "js/lib/",
  paths: {
    // dependencies
    // currently, some have pinned versions
    jquery:     "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min",
    cytoscape:  "https://cdnjs.cloudflare.com/ajax/libs/cytoscape/3.5.0/cytoscape.min",
    nouislider: "https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/13.1.2/nouislider.min",
    list:       "https://cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min",
    interact:   "https://unpkg.com/interactjs@next/dist/interact.min",
    NGL:        "https://unpkg.com/ngl@0.10.4/dist/ngl",
    logger:     "https://unpkg.com/js-logger/src/logger.min",
  }
});

define(["cytostruct", "sali", "logger", "data", "utils"],
  function (cytostructC, sali, Logger, dataobj, utils) {
    Logger.useDefaults();
    // a convention to enable debug level logging by setting a cookie
    if (document.cookie.indexOf("LOGVERBOSE") != -1) {
      Logger.setLevel(Logger.DEBUG);
    }
    else {
      Logger.setLevel(Logger.INFO);
    }
    return {
      cytostruct: cytostructC,
      seq_ali: sali,
      logger: Logger,
      data: dataobj,
      utilities: utils
    };
  });
