"use strict";
define(["cytoscape"],
  function(cytoscapeCtor) {
    // call Cytoscape constructor, after assembling a config object from given parts
    let _cyctor = function(div_element, elements_json, style_json, layout_js) {
      return cytoscapeCtor({
        container: div_element,
        elements: elements_json,
        ready: function(){},
        style: style_json,
        layout: layout_js
      });
    };

    return {
      // Cytoscape handle, populated after construction by cy_embed_in_div
      // (use it to call all the methods that are not exposed by this wrapper)
      _cyviewerobj: {},
      // container for removed elements, needed if we wish to restore
      _removed: [],

      // Cytoscape takes control of the div for network rendering
      cy_embed_in_div(div_id, elements_json, style_json, layout_js) {
        this._cyviewerobj = _cyctor(div_id, elements_json, style_json, layout_js);
      },

      // ==== Expose Cytoscape's methods ==================
      // on, so we can attach on-edge and on-node handlers
      // example: cy_on("tap", "node", callback)
      cy_on(event, element, cb) { this._cyviewerobj.on(event, element, cb); },
      // center view to given elements
      cy_center(eles) { this._cyviewerobj.center(eles);},
      // zoom using options object. example: zoom({level: 2})
      cy_zoom(opts) { this._cyviewerobj.zoom(opts); },
      // apply filter/selector to get elements, alias to $(selector)
      // example: cy_filter(`edge[evalue>${threshold}]`)
      cy_filter(fltr) { return this._cyviewerobj.filter(fltr); },
      // hide (not remove) all elements matching the given selector
      cy_hide_for_selector(selector) { this._cyviewerobj.$(selector).hide(); },
      // show (unless removed) all elements matching the given selector
      cy_show_for_selector(selector) { this._cyviewerobj.$(selector).show(); },
      // hide all nodes with degree (currently) <= than given
      cy_hide_le_than_degree(degree) { this.cy_hide_for_selector(`node[[degree <= ${degree}]]`); },
      // show all nodes with degree (currently) <= than given
      cy_show_le_than_degree(degree) { this.cy_show_for_selector(`node[[degree <= ${degree}]]`); },
      // hide all nodes with degree (currently) >= than given
      cy_hide_ge_than_degree(degree) { this.cy_hide_for_selector(`node[[degree >= ${degree}]]`); },
      // show all nodes with degree (currently) >= than given
      cy_show_ge_than_degree(degree) { this.cy_show_for_selector(`node[[degree >= ${degree}]]`); },
      // hide all nodes with no edges (currently)
      cy_hide_orphans() { this.cy_hide_le_than_degree(0); },
      // show all nodes with no edges (currently)
      cy_show_orphans() { this.cy_show_le_than_degree(0); },
      // show only the direct neighbors of the selected element
      cy_show_only_neighbors() {
        this._cyviewerobj.$(":selected").closedNeighborhood().absoluteComplement().hide();
      },
      // undo showing just the direct neighbors
      cy_show_non_neighbors() {
        this._cyviewerobj.$(":selected").closedNeighborhood().absoluteComplement().show();
      },
      // use the expression to filter and remove elements
      //e.g., "edge[" + g_edgefilterby + " > " + threshold +']'
      cy_update_edge_remove(expression) {
        this._removed.restore();
        this._removed = this._cyviewerobj.filter(expression).remove();
      }
    };
  });
