"use strict";
define(["jquery", "NGL", "logger"],
  function($, NGL, Logger) {

    return class ResourceLoader {
      constructor() {
        this.logger = Logger.get("file_loading");
      }

      // NGL params:
      // http://nglviewer.org/ngl/api/typedef/index.html#static-typedef-LoaderParameters
      //    ext	String	file extension, determines file type
      //    compressed	Boolean flag data as compressed
      //    binary	Boolean flag data as binary
      //    name	String set data name

      // Only works if the urls are in the same origin,
      // or other domain allows cross origin.
      // Will work for compressed structures too.
      // returns a Promise.all for the urls that resolves to an array of structs
      load_structures_from_server(urls, ngl_params) {
        this.logger.debug(`load_structures_from_server for urls: ${urls} with ${ngl_params}`);
        return Promise.all(urls.map(url => NGL.autoLoad(url, ngl_params)));
      }

      // Only works if the urls are in the same origin,
      // or other domain allows cross origin.
      // Use this to load compressed (json and plain-text) from a server.
      // returns a Promise.all for the urls that resolves to an array of data
      // ngl_params: {compressed: "gz"} <- actually usually detects everythin correctly ;)
      // ngl uses pako_inflate library to deal with zips:
      // see http://nglviewer.org/ngl/api/file/src/utils/gzip-decompressor.js.html
      // your ngl_params should probably be {compressed: "gz", ext: "json"}
      load_compressed_data_from_server(urls, ngl_params) {
        this.logger.debug(`load_compressed_data_from_server for urls: ${urls}`);
        this.logger.debug(NGL.getFileInfo(urls[0]));
        return Promise.all(urls.map(url => NGL.autoLoad(url, ngl_params)));
      }

      // require an AMD module, works for file:// and cross origin
      // returns a Promise for the urls that resolves to an array of modules
      require_amd_js(urls) {
        return new Promise((resolve, reject) => {
          this.logger.debug(`require for urls: ${urls}`);
          require(urls,
            function(...args){ resolve(...args);},
            function(err) { reject(err);}
          );
        });
      }

      // Use ajax to GET from given url.
      // Only works if the url is in the same origin, or allows cross origin.
      // Expects a single url,
      // and an optional http://api.jquery.com/jquery.ajax/ settings object.
      // returns a promise that resolves to (usually) not-parsed data,
      // unless it is very obvious from the url, and then it will try to...
      fetch_resource(url, settings = {}) {
        return new Promise((resolve, reject) => {
          this.logger.debug(`fetch for urls: ${url} with ${settings}`);
          $.ajax(url, settings)
            .done((...args) => {resolve(...args);})
            .fail((...args) => {reject(...args);});
        });
      }

      // 1. try static resource (with require)
      // 2. if not available, try to call a cgi to fetch it with ajax
      //    the cgi may be able to create a static copy in seconds
      //    3.1 if $.get was a success, use the data
      //    note: will parse the data as json
      //    3.2 if $.get fails, reject
      require_amd_or_fallback_to_json(amd_url, fallback_url) {
        return new Promise((resolve, reject) => {
          this.logger.debug(`require_amd_or_fallback_to_json for: '${amd_url}' and '${fallback_url}'`);
          require([amd_url],
            function(...args){ resolve(...args);},
            err => {
              this.logger.info(`Require error: ${err}. Retrying with ajax.`);
              this.fetch_resource(fallback_url, {dataType: "json"})
                .then(data => {resolve(data);})
                .fail(err => { reject(err);});
            }
          );
        });
      }
    };
  });
