"use strict";
// we want to have three "layers":
// 1. basic, somewhat transparent and pale -- for all atoms
// 2. alignment segments opaque and bold.
// 3. single "picked" residue
define(["NGL", "logger"], function(NGL, Logger) {

  // representations in order of preference
  const _representations = ["cartoon", "rope", "licorice"];
  const _base_colors = ["#d5edeb", "#efbaed"];
  const _alignment_colors = ["cyan", "magenta"];
  const _selection_repr = "ball+stick";
  const _selection_color = "orange";

  const _default_background = "white";
  const _default_width = 400;
  const _default_height = 400;

  const logger = Logger.get("molviewer");

  // try to represent the structure using given color scheme
  // cartoon may fail (bug open), then we continue to rope
  function _represent(structure, scheme, fallback_color = "orange") {
    for(let i in _representations) {
      try{
        return structure.addRepresentation(_representations[i], { color: scheme } );
      }
      catch(err) {
        logger.warn("Can't color-scheme with " + _representations[i] + ": " + err);
      }
    }
    for(let i in _representations) {
      try{
        return structure.addRepresentation(
          _representations[i], 
          {color: fallback_color});
      }
      catch(err) {
        logger.warn("Can't " + _representations[i] + ": " + err);
      }
    }

  }

  function _color_structs(
    comp_arr, // components
    residues_arr, // residues to color
    should_use_alignment_for_superimpose,
    color_arr = _base_colors) // colors for components representation
  {
    let hover_representations = {};
    const selection_colors = _alignment_colors;
    for(let i in comp_arr) {
      const colors = [[color_arr[i],"*"]];
      if(i < residues_arr.length) {
        colors.unshift([selection_colors[i], residues_arr[i]]);
      }
      logger.debug(colors);
      const scheme =
        NGL.ColormakerRegistry.addSelectionScheme(colors, "color" + i);
      _represent(comp_arr[i], scheme, color_arr[i]);

      const hrepr = comp_arr[i].addRepresentation(_selection_repr);
      hrepr.setSelection("-1000");
      hrepr.setColor(_selection_color);
      hover_representations[comp_arr[i].structure.id] = hrepr;
    }

    let first_comp = comp_arr.shift();
    if(should_use_alignment_for_superimpose) {
      for(let i in comp_arr) {
        first_comp.superpose(
          comp_arr[i], true,
          residues_arr[0], residues_arr[parseInt(i) + 1]);
      }
    }
    else {
      for(const i in comp_arr) { first_comp.superpose(comp_arr[i], true); }
    }
    first_comp.autoView(); //centerView(false,resList[0]);
    return hover_representations;
  }

  return {
    _molviewer: {},
    _hovered: {},
    should_use_alignement_in_superimpose: true,

    get_ngl() {
      return NGL;
    },

    // removes all proteins from the viewer
    mv_remove_all_comps() {
      this._molviewer.removeAllComponents();
    },

    // set flag -- using alignment is usually makes a better image,
    // however, if the alignment is off, disabling makes superimposing better.
    mv_use_alignment_in_superimposing(is_on) {
      this.should_use_alignement_in_superimpose = is_on;
    },

    // embed molecular viewer in user provided div (given by id)
    mv_embed_in_div(div_id, width=_default_width, height=_default_height){
      let stage = new NGL.Stage(div_id,
        { backgroundColor: _default_background } );
      this._molviewer = stage;
      stage.viewer.setSize(width, height);
      // killing built-in tooltip, replacing with our tooltip for now
      stage.tooltip.style = "display: none; font-size: 1%; color: AliceBlue"; 
    },

    // add a callback on-hover of atoms in the viewer
    mv_set_on_hover(callback) {
      this._molviewer.signals.hovered.add(callback);
    },
    
    // async load parsed blobs into NGL stage
    mv_load_structures_from_blobs(structs) {
      let that = this;
      return structs.map(function(strctr){
        return that._molviewer.addComponentFromObject(strctr);
      });
    },

    // async load text into NGL stage (blobs != NGL.Blobs)
    mv_load_structures_from_text(structs, ngl_params) {
      const load_promises = structs.map(strct => 
        this._molviewer.loadFile(
          new Blob([strct], {type: "text/plain"}), ngl_params));
      return Promise.all(load_promises);
    },

    // color a residue (by inex) in one of the loaded proteins (by name)
    mv_color_residue(residue, name) {
      let selection = [residue, residue].join("-");
      if(name in this._hovered) {
        this._hovered[name].setSelection(selection);
        this._hovered[name].update();
      }
      else {
        logger.error(name, "not in ", this._hovered);
      }
    },

    // Given lists of components and list of selected residues, 
    // show in the viewer. If more than one component, will superimpose them.
    mv_represent_components(components, residues) {
      // TODO: introduce representation object      
      this._hovered = _color_structs(
        components, residues, this.should_use_alignement_in_superimpose);
    },

    // resize, e.g., when containing div grows/shrinks
    mv_set_size(w,h) { this._molviewer.viewer.setSize(w,h); },

    // <static> given a string with protein_chain create a url (e.g. 1MSN_A)
    // to open coordinate server copy of that protein.chain
    mv_form_url_ncbr(prot_chain) {
      const ar = prot_chain.split("_");
      return "https://webchem.ncbr.muni.cz/CoordinateServer/" + ar[0].toLowerCase()
          +  "/chains?authAsymId=" + ar[1] + "&modelId=1";
    },

    // override the two form_url_* functions to configure id -> struct. mapping
    // both edge and node version need to return a url to structure(s)
    // by default uses ncbr coordinate server
    mv_form_url_edge(srcid, trgtid) {
      return [
        this.mv_form_url_ncbr(srcid),
        this.mv_form_url_ncbr(trgtid),
      ];
    }, 
    mv_form_url_node(nodeid) {
      return [
        this.mv_form_url_ncbr(nodeid)
      ];
    },

    // use the given element (node/edge) 
    // and call the default or set form_url methods
    mv_form_url(element) {
      if(element.group() === "edges") {
        const ar = element.data().name.split(" ");
        const srcid = ar[0]; //element.source().id().toLowerCase();
        const trgtid = ar[2]; //element.target().id().toLowerCase();
        return this.mv_form_url_edge(srcid, trgtid);
      }
      else  {
        const srcid = element.data().name;
        return this.mv_form_url_node(srcid);
      }
    }
  };
});
