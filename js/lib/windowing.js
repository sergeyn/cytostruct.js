"use strict";
define(["jquery", "interact", "logger"], function($, interact, Logger) {
  return class Windowing {
    constructor(cytohandle = undefined) {
      this.cytohandle = cytohandle;
      this.logger = Logger.get("cytostruct");
      this.win_registry = new Map();
    }

    // add toggler (checkbox) with label and value to whereTo $(element)
    // onChange is a callback(isChecked) to execute when the checkbox is clicked/triggered
    // ! the state of the element is already handled, prior to calling it
    appendToggler(whereTo, label, value, isChecked, onChange) {
      let chk = $(`<input type="checkbox" ${(isChecked ? "checked" : "")} value="${value}">`);
      chk.change(function(){
        const el = $(this);
        if(el.is(":checked")) {
          el.attr("checked", false);
          onChange(false, el.val());
        }
        else {
          el.attr("checked", true);
          onChange(true, el.val());
        }
      });
      whereTo.append(chk);
      whereTo.append($(`<label>${label}</label>`));
      return chk;
    }

    static make_window_from_element(element, stopcb, count_windows=0) {
      let win = $("<div class=\"draggable viewportwindow\"></div>");
      let header = $("<span></span>");
      let close = $("<u>close</u>");
      close.css({"float": "right", "cursor" : "pointer"});
      close.click(function(){win.hide();}); // default behavior is to hide
      let hwrap = $("<div class=\"panel-heading\"></div>");
      hwrap.css({
        "cursor": "move",
        "background-color": "lightblue",
        "margin": "1px",
      });
      win.css({
        "z-index": 100 + count_windows,
        "position": "absolute", "top" : "150px",
        "border" : "dashed", "border-width": "1px",
        "background" : "#fafafa"
      });
      hwrap.append(header);
      hwrap.append(close);
      win.prepend(hwrap);
      element = $(element);
      win.append(element);
      win.height(element.height() + 40);
      win.width(element.width() + 16);
      $("body").append(win);

      let translateWin = function(target, x, y) {
        // translate the element
        target.style.webkitTransform =
        target.style.transform =
          "translate(" + x + "px, " + y + "px)";
      };
      let dragMoveListener =  function(event) {
        var target = event.target,
          // keep the dragged position in the data-x/data-y attributes
          x = (parseFloat(target.getAttribute("data-x")) || 0) + event.dx,
          y = (parseFloat(target.getAttribute("data-y")) || 0) + event.dy;
          // update the posiion attributes
        target.setAttribute("data-x", x);
        target.setAttribute("data-y", y);

        translateWin(target, x, y);
      };

      interact(win.get()[0])
        .draggable({
          allowFrom: ".panel-heading",
          restrict: {
            restriction: "parent",
          },
          onmove: dragMoveListener
        })
        .resizable({
          edges: { right: true, bottom: true }
        })
        .on("resizemove", function (event) {
          var target = event.target,
            x = (parseFloat(target.getAttribute("data-x")) || 0),
            y = (parseFloat(target.getAttribute("data-y")) || 0);

          // update the element's style
          target.style.width  = event.rect.width + "px";
          target.style.height = event.rect.height + "px";

          // translate when resizing from top or left edges
          x += event.deltaRect.left;
          y += event.deltaRect.top;

          target.style.webkitTransform = target.style.transform =
              "translate(" + x + "px," + y + "px)";

          target.setAttribute("data-x", x);
          target.setAttribute("data-y", y);
          stopcb(event);
        });
      translateWin(win.get()[0], 20*count_windows+2, 20*count_windows+2);
      return [win, header, element, close];
    }
    //--------------------------------------------------------------------------
    // given an $(element), make it draggable, resizable with header
    // stopcb is a callback to run when resizing of the element stops
    // returns the window structure -- [win, header, element, close]
    make_window(element, stopcb) {
      this.logger.debug("Making window from ", element);
      return Windowing.make_window_from_element(element, stopcb, this.win_registry.size);
    }

    // check if window registry has one with given name
    has_registry(name) {
      return this.win_registry.has(name); }
    // fetch a window structure from the registry
    get_from_registry(name) { return this.win_registry.get(name);}
    // add window structure to registry under given name
    add_to_registry(name, element_data) {
      this.win_registry.set(name, element_data);
      return element_data;
    }
    // remove window structure from registry for given name
    rm_from_registry(name) { this.win_registry.delete(name); }

    //--------------------------------------------------------------------------
    // run nillary/unary jquery method on a window (given by name)
    // examples:
    // jq_func("hide", "lens")
    // jq_func("append", "toolbar", $("<div>foo</div>"));
    jq_func(func, win_name, arg = {}) {
      if(this.has_registry(win_name)) {
        $(this.get_from_registry(win_name))[func](arg);
      }
      else {
        this.logger.debug(`No window '${win_name}' registered`);
      }
    }

  };
});
