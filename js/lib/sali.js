"use strict";
define(function() {
  const GAP = "-";

  // produce a span html string from 2 opposing residues <residue, other>
  let residue = function(index, res, other) {
    let cls = "residue";
    if(res === other) {
      cls += " residue_identical";
    }
    return `<span id="${index}" class="${cls}">${res}</span>`;
  };

  // calculate "real" offset, ignoring gaps
  // residue number given by mol. viewer,
  // which operates on a residues sequence with no gaps and starts with 1
  // if other deletions take place, this will be somewhat off,
  // for better results, augment the residues with our .aln data
  // NB: could be done ahead of time and cached
  let correct_offset = function(resnum, offset, residues_arr) {
    if(resnum < offset || resnum > offset + residues_arr.length)
      return -9999;
    // residues are 1-based, but so are the offsets
    for(let i in residues_arr) {
      if(offset === resnum){
        return i;
      }
      if(residues_arr[i] !== GAP) {
        ++offset;
      }
    }
  };

  return class Sali {
    constructor(element, lname, rname, lresi, rresi, loffset, roffset) {
      this.l_residues = lresi.split("");
      this.r_residues = rresi.split("");
      this.l_offset = loffset;
      this.r_offset = roffset;
      this.l_name = lname;
      this.r_name = rname;
      this.domelement = element;
      this.onhover = function(){};
      this.onclick = function(){};
    }

    // remove existing alignement from the div
    clear() {
      let element = this.domelement;
      element.html("<div id=\"lresidues\"></div><div id=\"rresidues\"></div>");
    }

    // Draw the alignment in the given element
    render() {
      const lresidues = this.l_residues.map(
        (res, index) => {
          return residue("lrsd_" + index, res, this.r_residues[index]);});
      const rresidues = this.r_residues.map(
        (res, index) => {
          return residue("rrsd_" + index, res, this.l_residues[index]);});

      let element = this.domelement;
      element.html(
        `<div id="lresidues">${lresidues.join("")}</div>
         <div id="rresidues">${rresidues.join("")}</div>`);
      element.off();
      element.on("mouseenter", ".residue", {obj: this},
        function(event) {
          let idarr = event.target.id.split("_");
          let l_or_r = idarr[0][0];
          let saliself = event.data.obj;
          let offset = saliself.offset(l_or_r);
          saliself.call_onhover(offset + parseInt(idarr[1]), saliself.name(l_or_r));
        });
    }

    // select the right residues when given id (name of the sequence)
    // and resnum -- the residue we are hovering over in molecular viewer
    // if resnum < 0 just clear
    select(id, resnum) {
      let res_data =
        id === this.l_name ?
          [ this.l_offset, this.l_residues] :
          [ this.r_offset, this.r_residues];

      $(".residue").removeClass("residue_hover");
      if(resnum >= 0) {
        // perform translation from residue number from mol. viewer,
        // which has no gaps and starts with 1
        const offset = correct_offset(resnum, res_data[0], res_data[1]);
        $("#lresidues :eq(" + offset + ")").addClass("residue_hover");
        $("#rresidues :eq(" + offset + ")").addClass("residue_hover");
      }
    }

    name(left_or_right) {
      return left_or_right === "l" ? this.l_name : this.r_name;
    }

    offset(left_or_right) {
      return left_or_right === "l" ? this.l_offset : this.r_offset;
    }

    // return a pair of residues [l,r] - use "local" 0-based index
    at(index) { return this.l_residues[index], this.r_residues[index]; }

    // return the residue at absolute offset (FASTA index)
    left_at(offset) { return this.l_residues[offset - this.l_offset];}

    right_at(offset) { return this.r_residues[offset - this.r_offset];}
    length() { return this.l_residues.length;}
    call_onhover(offset, name) { this.onhover(offset, name); }
    set_on_hover(callback) { this.onhover = callback; }
    set_on_click(callback) { this.onclick = callback; }
  };
});
