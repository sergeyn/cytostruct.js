"use strict";
define(["jquery", "data"],
  function($, data) {
    //const logger = Logger.get("utils");

    let _handle = {
      get_jquery: function() {
        return $;
      },

      //--------------------------------------------------------------------------
      // create a lens window using win. manager
      make_lens_window: function(wm) {
        let lendiv = $("<div></div>");
        lendiv.css({display: "block", width: "400px", height: "500px", "z-index": 101, "overflow-y" : "auto"});
        $("body").append(lendiv);
        let [win, lheader, body, close] = wm.make_window(lendiv, function(e) {
          lendiv.width(e.rect.width);
          lendiv.height(e.rect.height-10);
        });
        lheader.html("Lens");
        win.hide();
        wm.add_to_registry("lens", win);
        wm.add_to_registry("lens_header", lheader);
        return [win, lheader, body, close];
      },
      //--------------------------------------------------------------------------
      // create alignment window using win. manager
      make_alignment_window: function(wm) {
        let saliwin = document.createElement("div");
        Object.assign(saliwin.style, {
          "width": "100%",
          "overflow-x": "scroll",
          "overflow-y": "hidden",
          "white-space": "nowrap"
        });
        document.body.appendChild(saliwin);
        let [win, lheader, body, close] = wm.make_window(saliwin, function() {});
        lheader.html("Alignment view");
        win.width(800); win.height(85); 
        return [win, lheader, body, close];
      },

      sali_default_handler: function(
        cyelement, cytostruct, sali, onhover, // <- callback for onhover
        // element fields mapping
        srcid = "", trgtid = "", res1 = "Res1", res2 = "Res2", ali1 = "Ali1", ali2 = "Ali2") {

        const wm = cytostruct.get_win_manager();
        if(!wm.has_registry("sali_win")) {
          let elements = this.make_alignment_window(wm);
          wm.add_to_registry("sali_win", elements);
          elements[3].click(() => wm.rm_from_registry("sali_win"));
        }
        if(srcid === "") srcid = cyelement.source().data().name.split("_")[0]; 
        if(trgtid === "") trgtid = cyelement.target().data().name.split("_")[0]; 
        let cyeldata = cyelement.data();
        let alignment = new sali(
          $(wm.get_from_registry("sali_win")[2]),
          srcid,
          trgtid,
          cyeldata[ali1],
          cyeldata[ali2],
          parseInt(cyeldata[res1].split("-")[0]),
          parseInt(cyeldata[res2].split("-")[0]));
        alignment.render();
        const sanity_handler = (r,n) => cytostruct.mv_color_residue(r,n);
        let hover_handler =  onhover || sanity_handler;
        alignment.set_on_hover(hover_handler);
        cytostruct.alignment = alignment;
      },
      //--------------------------------------------------------------------------
      // create tooltip hovering widget
      make_tooltip: function() {
        // create tooltip element and add to document body
        var tooltip = document.createElement("div");
        Object.assign(tooltip.style, {
          display: "none",
          position: "fixed",
          zIndex: 101,
          pointerEvents: "none",
          backgroundColor: "rgba( 0, 0, 0, 0.6 )",
          color: "lightgrey",
          padding: "8px",
          fontFamily: "sans-serif"
        });
        document.body.appendChild(tooltip);
        return tooltip;
      },
      //--------------------------------------------------------------------------
      // convert location hash to, well, a hash
      location_fields: function() {
        let fields = new Map();
        location.hash.replace("#","").split("&")
          .map(x => x.split("=")).forEach(x => fields.set(x[0], x[1]));
        return fields;
      },
      // and back
      fields_to_location: function(fields) {
        let parts = [];
        for (let [key, value] of fields) {
          parts.push(`${key}=${value}`);
        }
        location.hash = "#" + parts.join("&");
      },
      //--------------------------------------------------------------------------
      // get all e_json[type]["data"][value_tag] sorted ascending
      get_values_from_elements: function(e_json, type, value_tag) {
        if(e_json.hasOwnProperty(type)) {
          return e_json[type]
            .map(item => item["data"][value_tag])
            .sort(function(a,b) { return a - b;});
        }
        return [0, 1000];
      },
      //--------------------------------------------------------------------------
      // get all evalues of edges as a sorted array
      get_evalues_from_elements: function(e_json, evalue_tag = "evalue") {
        return this.get_values_from_elements(e_json, "edges", evalue_tag);
      },
      //-----------------------------------------------------------------------
      zoom_to_node_by_name: function(cy, nodename) {
        let node = cy.cy_filter(`node[name = "${nodename}"]`);
        if(node && node.length > 0) {
          cy.toggle_neighbors({checked: false});
          cy.cy_center(node);
          cy.cy_zoom({ level: 2, position: node.position()});
          node.select();
        }
        return node;
      },
      //--------------------------------------------------------------------------
      // return a callback to handle hovering in mv
      // will update: alignment (if any), mv, tooltip (if exists)
      get_default_hover: function(cy) {
        const default_hover = function(picking){
          if (picking && (picking.atom || picking.bond)) {
            let atom = picking.atom || picking.closestBondAtom;

            cy.alignment.select(atom.structure.id, atom.resno);
            cy.mv_color_residue(atom.resno, atom.structure.id);

            let mp = picking.mouse.position;
            let tooltip = cy.get_win_manager().get_from_registry("tooltip");
            tooltip.innerText =
                "Residue: " + atom.resname + "/" +
                data.astrals[atom.resname] +
                "(" + atom.resno + "~" + atom.residueIndex + ")";
            // mp.x and mp.y are in window coordinates
            tooltip.style.bottom = window.innerHeight - mp.y + 15 + "px";
            tooltip.style.left = mp.x + 15 + "px";
            tooltip.style.display = "block";
          } else {
            cy.get_win_manager().get_from_registry("tooltip").style.display = "none";
          }
          
        };
        return default_hover;
      },  
      //--------------------------------------------------------------------------
      // return a dom element with search widget for zooming to nodes
      // note, the network needs to be loaded into cytoscape at this point,
      // since we fetch the node names from it
      build_search_bar: function(cy) {
        let search =  $("<input type=\"button\" value=\"&larr; Zoom to node\">");
        let input = $("<input type=\"text\" list=\"node-search-datalist\">");
        let datalist = $("<datalist id=\"node-search-datalist\"></datalist>");
        cy.cy_filter("node").forEach(function(nd){
          datalist.append($(`<option value="${nd.data().name}">`));
        });

        let search_handler = () => this.zoom_to_node_by_name(cy, input.val());
        input.on("keyup", function(e) { if(e.keyCode == 13) { search_handler(); }});
        search.click(search_handler);

        let spn = $("<span></span>");
        spn.append(input);
        spn.append(search);
        spn.append(datalist);
        spn.prependTo(document.body);
        return spn;
      }
    };
    return _handle;
  });
