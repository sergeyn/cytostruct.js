"use strict";
define(["aux/ecod_to_pdbnum"], function(ecod_mapping) {
  return function(cytostruct, sali, Logger, dataobj, utils, fields) {
    let logger = Logger.get("blogic");
    let win_man = cytostruct.get_win_manager();

    const default_layout = { name: "concentric" };
    const basecgi_local = "delegate.php?action=ecod";
    const basecgi = "get.php?action=ecod";
    const lens_href = "http://prodata.swmed.edu/ecod/complete/domain/";

    let ASTRALS = dataobj.astrals;

    //-----------------------------------------------------------------------
    // when hovering inside mol. viewer
    let default_hover = function(picking){
      if (picking && (picking.atom || picking.bond)) {
        const atom = picking.atom || picking.closestBondAtom;

        cytostruct.alignment.select(atom.structure.id, atom.resno);
        cytostruct.color_residue(atom.resno, atom.structure.id);

        const mp = picking.mouse.position;
        //const offset = cytostruct.molv_coordinates();
        const tooltip = win_man.get_from_registry("tooltip");
        tooltip.innerText =
          "Residue: " + atom.resname + "/" +
          ASTRALS[atom.resname] +
          "(" + atom.resno + "~" + atom.residueIndex + ")";
        tooltip.style.bottom = window.innerHeight - mp.y - 15 + "px";
        tooltip.style.left = mp.x + 3 + "px";
        tooltip.style.display = "block";
      } else {
        win_man.get_from_registry("tooltip").style.display = "none";
      }
    };
    //-----------------------------------------------------------------------
    // clicking node or edge in cytoscape
    let default_handler = function(evt){
      let cyelement = evt.target;
      let struct_type = "pdb";

      cytostruct.remove_all_comps();

      cytostruct.load_files(cytostruct.form_url(cyelement), struct_type)
        .then(function(structs) {
          return cytostruct.load_structures(structs);})
        .then(function(structs){
          let resList = [
            String(cyelement.data()["Res1"]).replace(/:/g,"-").replace(/,/g," or "),
            String(cyelement.data()["Res2"]).replace(/:/g,"-").replace(/,/g," or ")
          ];
          cytostruct.represent_components(structs, resList);
        });

      cytostruct.mv_set_on_hover(default_hover);

      if(cyelement.group() === "nodes") {
        let nodename = cyelement.data().name;
        cytostruct.change_molviewer_title(nodename);
        populate_lens(cyelement);

        let fields = utils.location_fields();
        fields.set("node", nodename);
        fields.delete("source");
        fields.delete("target");
        utils.fields_to_location(fields);
      }
      else if(cyelement.group() === "edges") {
        let source = cyelement.data().source;
        let target = cyelement.data().target;
        cytostruct.change_molviewer_title(source + " - " + target);
        // in this example we use ecod names e4oc6A1,
        // but the proteins report to NGL 4OC6,
        // hence we wrap the default handler
        let hover = (residue, name) => {
          cytostruct.color_residue(residue, name.slice(1,5).toUpperCase());};
        utils.sali_default_handler(cyelement, cytostruct, sali, hover);

        let fields = utils.location_fields();
        fields.delete("node");
        fields.set("source", source);
        fields.set("target", target);
        utils.fields_to_location(fields);
      }
      $(win_man.get_from_registry("mv")[0]).show();
    };

    //-----------------------------------------------------------------------
    let populate_lens = function(node) {
      let arr_neigh = node.connectedEdges().map(function(edge) {
        let src = edge.source();
        let tgt = edge.target();
        let other = (src === node) ? tgt : src;
        return {name: other.data().name, value: edge.data().evalue };
      }).sort(function(a,b) { return a.value - b.value;}).map(function(obj){
        return `<li><a href="${lens_href + obj.name}">${obj.name}</a>&nbsp;${obj.value}</li>`;
      });

      let [lens_win, lens_header, lens_body] = win_man.get_from_registry("lens");
      let name = node.data().name;
      lens_header.html(name);
      lens_body.html(
        `<p><h3>&nbsp;ECODs</h3><ul><a href="${lens_href + name}">${name}</a></ul></p>
        <div><ul style="list-style-type: none">${arr_neigh.join("\n")}</ul></div>`);
      lens_win.show();
    };

    //-----------------------------------------------------------------------
    let zoom_to_node = function(node) {
      cytostruct.toggle_neighbors({checked: false});
      cytostruct.cy_center(node);
      cytostruct.cy_zoom({ level: 2, position: node.position()});
      node.select();
      //toggler.prop("checked", true);
      //cytostruct.show_only_neighbors();
      populate_lens(node);
    };

    let zoom_to_node_by_name = function(nodename) {
      let node =
        cytostruct.cy_filter(`node[name = "${nodename}"]`);
      if(node && node.length > 0) {
        zoom_to_node(node);
      }
      return node;
    };
    //-----------------------------------------------------------------------
    let build_search_bar = function() {
      let search =  $("<input type=\"button\" value=\"&larr; Zoom to domain\">");
      let input = $("<input type=\"text\" list=\"node-search-datalist\">");
      let datalist = $("<datalist id=\"node-search-datalist\"></datalist>");
      cytostruct.cy_filter("node").forEach(function(nd){
        datalist.append($(`<option value="${nd.data().name}">`));
      });

      let search_handler = function() { zoom_to_node_by_name(input.val()); };
      input.on("keyup", function(e) { if(e.keyCode == 13) { search_handler(); }});
      search.click(search_handler);

      let spn = $("<span></span>");
      spn.append(input);
      spn.append(search);
      spn.append(datalist);
      return spn;
    };

    //-----------------------------------------------------------------------
    let add_node_togglers = function(cytostruct, toolbar_div, wm) {
      return [
        wm.appendToggler(
          toolbar_div,
          "Hide orphans", 0, false,
          isC => cytostruct.toggle_orphans({checked: !isC})),

        wm.appendToggler(
          toolbar_div,
          "Hide non-neighbors", 1, false,
          isC => cytostruct.toggle_neighbors({checked: !isC}))
      ];
    };

    //-----------------------------------------------------------------------
    let populate_toolbar = function(cytostruct) {
      let toolbar_div = win_man.get_from_registry("toolbar");
      let node_togglers = add_node_togglers(cytostruct, toolbar_div, win_man);
      toolbar_div.append(build_search_bar(node_togglers[1]));
      cytostruct.set_on("unselect", "node", function() {
        node_togglers[1].prop("checked", false);
        cytostruct.show_non_neighbors();
      });

      win_man.appendToggler(toolbar_div,
        "Alignment based superimpose", 0, true,
        isC => cytostruct.use_ali_in_super(!isC));

      win_man.add_to_registry(
        "lens", utils.make_lens_window(win_man));
    };

    //-----------------------------------------------------------------------
    let load_from_json = function(cytohandle, net) {
      const layout = default_layout;
      cytohandle.cs_embed_in_div(
        "body", net.elements, net.style, layout,
        {lens: true, slider: true, mv: true});
      populate_toolbar(cytostruct);
      cytohandle.set_tap_node(default_handler);
      cytohandle.set_tap_edge(default_handler);
      window.cytostructjs.showing = true;

      let fields = utils.location_fields();
      if(fields.has("node")) {
        logger.debug("Using node: " + fields.get("node"));
        let node = cytostruct.cy_filter("node[name = \"" + fields.get("node") + "\"]");
        zoom_to_node(node);
        default_handler({target: node});
      }
      else if(fields.has("source") && fields.has("target")) {
        logger.debug(`Using edge: ${fields.get("source")} - ${fields.get("target")}`);
        let edge =
          cytostruct.cy_filter(
            `edge[source = "${fields.get("source")}"][target = "${fields.get("target")}"]`);
        logger.debug(edge[0]);
        default_handler({target: edge[0]});
      }
    };

    //-----------------------------------------------------------------------
    let construct_static_name_and_url = function(fields, cgibase) {
      const name = fields.get("db") + "-" + fields.get("net");
      const evalue = (fields.has("eval")) ? fields.get("eval") : 0.1;
      const len = (fields.has("len")) ? fields.get("len") : 20;
      const static_name = `net/${name}__${evalue}__${len}`;

      const uri = `${cgibase}&target=${fields.get("net")}&db=${fields.get("db")}&eval=${evalue}&len=${len}`;
      logger.debug("Candidate: " + static_name + " aka " + uri);
      return [static_name, uri];
    };

    //-----------------------------------------------------------------------
    let fetch_json = function(fields, cgibase, callmeback, failcall) {
      $("#main").html("Loading the network. This may take a while if it is not cached.");
      // 1. try static resource (with require)
      // 2. if not available, try to call a cgi to fetch it with $.getJSON
      //    the cgi may be able to create a static copy in seconds
      //    3.1 if $.getJSON was a success, use the data
      //    3.2 if $.getJSON fails, .fail(function( jqxhr, textStatus, error )
      const path_arr = construct_static_name_and_url(fields, cgibase);
      const loader = cytostruct.get_file_loader();

      if (fields.has("force")) {
        loader.fetch_resource(path_arr[1], {dataType: "json"})
          .then(function(data) {
            logger.debug(`$.get success for ${path_arr[1]}`);
            callmeback(data);
          })
          .catch(failcall);
      }
      else {
        loader.require_amd_or_fallback_to_json(path_arr[0], path_arr[1])
          .then(data => { callmeback(data); })
          .catch(failcall);
      }
    };

    //-----------------------------------------------------------------------
    cytostruct.form_url = function(element) {
      if(element.group() === "nodes") {
        let srcid = element.data().name;
        let pdbnum = ecod_mapping[srcid];
        logger.debug("pdbnum for ecod: " + pdbnum);
        return ["trampoline.php?target=" + pdbnum + "/" + pdbnum + ".pdbnum.pdb"];
      }
      let srcpdb = ecod_mapping[element.data().source];
      let tgtpdb = ecod_mapping[element.data().target];
      return [
        "trampoline.php?target=" + srcpdb + "/" + srcpdb + ".pdbnum.pdb",
        "trampoline.php?target=" + tgtpdb + "/" + tgtpdb + ".pdbnum.pdb"
      ];
    };

    //-----------------------------------------------------------------------
    let cgi_url = window.location.href.includes("trachel-srv") ? basecgi : basecgi_local;
    fetch_json(fields, cgi_url,
      function(net) {
        $("#main").html("");
        load_from_json(cytostruct, net);
      },
      function(err) {
        logger.info(err);
        alert( "Could not get the network. Try again later." );
      });
  };
});
