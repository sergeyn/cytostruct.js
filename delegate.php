<?php
  // just calls get.php from trachel, mildly useful when running server on localhost
  $suffix = str_replace("delegate.php", "get.php", $_SERVER['REQUEST_URI']);
  $content = file_get_contents("http://trachel-srv.cs.haifa.ac.il/rachel/cytostruct.js" . $suffix);
  print_r($content);

  $static_name = "networks/" . $_GET['db'] . "-" . $_GET['target'] . "__" . $_GET['eval'] . "__" . $_GET['len'] . ".js";
  $handle2 = fopen($static_name, "w");
  fwrite($handle2, "define(function(require) { return");
  fwrite($handle2, $content);
  fwrite($handle2, ";});");
  fclose($handle2);
?>
