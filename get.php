<?php
  $content =
    "action=" . ($_GET['action'] ?? "fetch")
      . "&target=" . $_GET['target']
      . "&db=" . $_GET['db']
      . "&eval=" . $_GET['eval']
      . "&len=" . $_GET['len'];
  $opts = array(
    'http'=>array(
      'method'=>"POST",
      'header'=> "Content-type: application/x-www-form-urlencoded\r\n",
      'content' => $content));
  $context = stream_context_create($opts);

  $static_name = "networks/" . $_GET['db'] . "-" . $_GET['target'] . "__" . $_GET['eval'] . "__" . $_GET['len'] . ".js";
  $handle = fopen('http://cluster:9955', 'r', false, $context);
  if (!$handle) {
    header($http_response_header[0]);
    die("Error for $static_name: $http_response_header[0]");
  }
  else {
    header("Content-Type: application/json; charset=UTF-8");
    $handle2 = fopen($static_name, "w");
    fwrite($handle2, "define(function(require) { return");
    while(! feof($handle)) {
      $line = fgets($handle);
      echo $line;
      fwrite($handle2, $line);
    }
    fwrite($handle2, ";});");
    fclose($handle2);
    fclose($handle);
  }
?>
